{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module RulesTests (rulesTests) where

import Test.Tasty
import Test.Tasty.HUnit

import qualified Data.Vector as V
import qualified Data.Sequence as Seq

import Types
import Rules
import Parser
import Utils

rulesTests :: TestTree
rulesTests = testGroup "Rules Tests"
  [ testCase "MoveUp"    $ pos (snd $ act MoveUp    $ w defBot) @?= (5,6)
  , testCase "MoveDown"  $ pos (snd $ act MoveDown  $ w defBot) @?= (5,4)
  , testCase "MoveLeft"  $ pos (snd $ act MoveLeft  $ w defBot) @?= (4,5)
  , testCase "MoveRight" $ pos (snd $ act MoveRight $ w defBot) @?= (6,5)
  , testCase "D for DL"  $ not (actionAllowed world dlBot MoveDown ) @? "Not allowed"
  , testCase "L for DL"  $ not (actionAllowed world dlBot MoveLeft ) @? "Not allowed"
  , testCase "U for UR"  $ not (actionAllowed world urBot MoveUp   ) @? "Not allowed"
  , testCase "R for UR"  $ not (actionAllowed world urBot MoveRight) @? "Not allowed"
  , testCase "example1"  $ countEmpty (fst $ run (w1,b1) s1) @?= 0
  , testCase "example2"  $ countEmpty (fst $ run (w1,b1) s2) @?= 0
  , testCase "example3"  $ countEmpty (fst $ run (w1,b1) s3) @?= 0
  , testCase "hands init"$ botMainHandsPos b1 @?= RightOf
  , testCase "hande hoch"$ botMainHandsPos b1{hands = handsLeft(hands b1)} @?= Above
  , testCase "hands down"$ botMainHandsPos b1{hands = handsRight(hands b1)} @?= Below
  , testCase "hands init"$ botMainHandsPos b1{hands = handsLeft(handsRight(hands b1))} @?= RightOf
  ]
  where
    mkEmptyWorld maxX maxY =
      let worldMap = V.replicate maxY $ V.replicate maxX (Empty, Nothing)
          obstacles = []
          spawner = Nothing
          teleports = []
      in World {..}
    world = mkEmptyWorld 8 10
    mkBotState pos fastFor drillingFor = let
      hands = startHands
      boosters = [Extension, FastWheel, Drill]
      in BS{..}
    defBot = mkBotState (5,5) 0 0
    dlBot = mkBotState (0,0) 0 0
    urBot = mkBotState (7,9) 0 0
    w = (world,)
    (Right p1) = parseProblem prob1
    (Right s1) = parseSolution sol1
    (Right s2) = parseSolution sol2
    (Right s3) = parseSolution sol3
    w1 = problemToWorld p1
    b1 = problemToBotState p1
    run = foldl (\(wn,bn) a -> act a (wn,bn))

prob1 = "(0,0),(10,0),(10,10),(0,10)#(0,0)#(4,2),(6,2),(6,7),(4,7);(5,8),(6,8),(6,9),(5,9)#B(0,1);B(1,1);F(0,2);F(1,2);L(0,3);X(0,9)"
sol1 = "WDWB(1,2)DSQDB(-3,1)DDDWWWWWWWSSEDSSDWWESSSSSAAAAAQQWWWWWWW"
sol2 = "WWDSFDDDDAQWWWWQAAAQSSSQDWWDDDWSSS"
sol3 = "WDWAWSSFDDDDDQQWLAAAAAWEEDDDDDWQQAAAAAWEEDDDDD"

