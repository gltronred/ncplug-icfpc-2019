{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Test.Tasty
import Test.Tasty.HUnit

import qualified Data.Vector as V

import Types
import Utils
import Parser (parseProblem)
import Rules

import PathTests
import SearchTests
import RulesTests
import RectTests

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ insidePolyTests
  , loadTests
  , countTests
  , searchTests
  , rulesTests
  , pathTests
  , rectTests
  ]

insidePolyTests :: TestTree
insidePolyTests = testGroup "insidePoly"
  [ testCase "1,1" $ (1,1)`insidePoly` [ (0,0),(3,0),(3,2),(0,2) ] @? "! inside rect"
  , testCase "0,0" $ (0,0)`insidePoly` [ (0,0),(3,0),(3,2),(0,2) ] @? "! inside rect"
  , testCase "3,0" $ (3,0)`ninsidePoly`[ (0,0),(3,0),(3,2),(0,2) ] @? "inside rect"
  , testCase "0,2" $ (0,2)`ninsidePoly`[ (0,0),(3,0),(3,2),(0,2) ] @? "inside rect"
  , testCase "3,2" $ (3,2)`ninsidePoly`[ (0,0),(3,0),(3,2),(0,2) ] @? "inside rect"
  , testCase "5,8" $ (5,8)`insidePoly` [ (5,8),(6,8),(6,9),(5,9) ] @? "! inside rect"
  , testCase "6,8" $ (6,8)`ninsidePoly`[ (5,8),(6,8),(6,9),(5,9) ] @? "inside rect"
  , testCase "6,9" $ (6,9)`ninsidePoly`[ (5,8),(6,8),(6,9),(5,9) ] @? "inside rect"
  , testCase "5,9" $ (5,9)`ninsidePoly`[ (5,8),(6,8),(6,9),(5,9) ] @? "inside rect"
  ]
  where ninsidePoly p ps = not $ insidePoly p ps

loadTests :: TestTree
loadTests = testGroup "parseProblemFile example-01"
  [ testCase "Correct obstacles" $ let
      Right p = parseProblem "(0,0),(10,0),(10,10),(0,10)#(0,0)#(5,8),(6,8),(6,9),(5,9)#"
      w = problemToWorld p
    in map fst (filter ((==Stone).fst.snd) $ assocs $ worldMap w) @?= [(5,8)]
  ]

countTests :: TestTree
countTests = testGroup "Counts"
  [ testCase "All empty 2x2"   $ countEmpty (mkEmptyWorld 2 2)     @?= 4
  , testCase "All empty 3x2"   $ countEmpty (mkEmptyWorld 3 2)     @?= 6
  , testCase "All empty 3x5"   $ countEmpty (mkEmptyWorld 3 5)     @?= 15
  , testCase "All wrapped 3x2" $ countEmpty (mkWrappedWorld 3 2)   @?= 0
  , testCase "All wrapped 3x5" $ countEmpty (mkWrappedWorld 3 5)   @?= 0
  , testCase "All empty 3x2"   $ countWrapped (mkEmptyWorld 3 2)   @?= 0
  , testCase "All empty 3x5"   $ countWrapped (mkEmptyWorld 3 5)   @?= 0
  , testCase "All wrapped 3x2" $ countWrapped (mkWrappedWorld 3 2) @?= 6
  , testCase "All wrapped 3x5" $ countWrapped (mkWrappedWorld 3 5) @?= 15
  , testCase "Some empty initially"   $ countEmpty exampleWorld > 0 @? "All cells are empty from the beginning"
  , testCase "Three wrapped initially" $ countWrapped exampleWorld @?= 3 -- @? "Three cells should be wrapped from the beginning"
  , testCase "17 empty in prob-001" $ countEmpty prob001World @?= 17
  , testCase "WDDDD doesn't solve prob-001" $ let
      (w',b') = foldl actionOutcome (prob001World, prob001Bot) [MoveUp, MoveRight, MoveRight, MoveRight, MoveRight]
    in countEmpty w' @?= 3
  ]
  where
    mkEmptyWorld maxX maxY =
      let worldMap = V.replicate maxY $ V.replicate maxX (Empty, Nothing)
          obstacles = []
          teleports = []
          spawner = Nothing
      in World {..}
    mkWrappedWorld maxX maxY =
      let worldMap = V.replicate maxY $ V.replicate maxX (Wrapped, Nothing)
          spawner = Nothing
          teleports = []
          obstacles = []
      in World {..}
    Right example = parseProblem "(0,0),(10,0),(10,10),(0,10)#(0,0)#(4,2),(6,2),(6,7),(4,7);(5,8),(6,8),(6,9),(5,9)#B(0,1);B(1,1);F(0,2);F(1,2);L(0,3);X(0,9)"
    exampleWorld = problemToWorld example
    Right prob001 = parseProblem "(0,0),(6,0),(6,1),(8,1),(8,2),(6,2),(6,3),(0,3)#(0,0)##"
    prob001World = problemToWorld prob001
    prob001Bot = problemToBotState prob001

