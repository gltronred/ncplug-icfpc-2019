{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module RectTests (rectTests) where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import qualified Data.HashMap.Strict as M
import Data.Maybe
import qualified Data.Set as S
import qualified Data.Vector as V

import Types
import Utils
import HeurRect

instance Arbitrary Cell where
  arbitrary = frequency [(3,pure Empty), (0,pure Wrapped), (1,pure Stone)]

instance Arbitrary World where
  arbitrary = sized $ \n -> sized $ \m -> do
    ll <- V.fromList <$> vectorOf n (V.fromList <$> vectorOf m ((,Nothing) <$> arbitrary))
    pure $ World {worldMap = ll, maxX = m, maxY = n, obstacles = [], spawner = Nothing, teleports = []}

rectTests :: TestTree
rectTests =
  localOption (QuickCheckMaxSize 50) $
  -- localOption (QuickCheckTests 400) $
  testGroup "Rect Tests"
  [ testProperty "Rects are inside World" $ \w -> let
      rs = worldToRects w
      ps = concatMap (\((x0,y0),(xN,yN)) -> [(x,y) | x<-[x0..xN], y<-[y0..yN]]) rs
      in counterexample (show rs) $ all ((/=Stone) . fst . (worldMap w !@)) ps
  , testProperty "Rects cover World" $ \w -> let
      rs = worldToRects w
      ps = map fst $ filter ((/=Stone).fst.snd) $ assocs $ worldMap w
      in counterexample (show rs) $ all (\p -> any (p`insideRect`) rs) ps
  , testProperty "Rects do not intersect" $ \w -> let
      rs = worldToRects w
      pairs = [ (r1,r2) | r1 <- S.toList rs, r2 <- S.toList rs, r1/=r2 ]
      in counterexample (show rs) $ all (not . uncurry intersect) pairs
  , testCase "Test world 1 split into 6 rects" $ length rs1 @?= 6
  , testCase "Test world 2 split into 3 rects" $ length rs2 @?= 3
  , testCase "QC test world correctly maps to Set" $ let
      w = V.fromList $ map (V.fromList . map (,Nothing))
          [ [Stone,Empty,Empty]
          , [Empty,Empty,Empty]
          , [Stone,Empty,Empty]
          ]
      kvs = assocs w
      m = map fst $ filter ((/=Stone).fst.snd) kvs
    in m @?= [ (1,0), (2,0), (0,1), (1,1), (2,1), (1,2), (2,2)]
  ]
  where
    rs1 = worldToRects testWorld1
    rs2 = worldToRects testWorld2
    intersect r1 r2 = any (`insideRect` r1) (corners r2) || any (`insideRect` r2) (corners r1)
    corners ((l,b), (r,t)) = [(l,b), (r,b), (r,t), (l,t)]

testWorld1 :: World
testWorld1 = World{ worldMap = w, maxX = 5, maxY = 6, obstacles = [], spawner = Nothing , teleports = []}
    where
      w = V.generate 7 $ \y ->
        V.generate 6 $ \x ->
        if (x,y)`elem`coords then (Empty, Nothing) else (Stone, Nothing)
      coords = [(0,6), (0,3), (1,3), (1,0), (5,0), (5,4), (4,4), (4,6)]

testWorld2 :: World
testWorld2 = problemToWorld $ Problem coords (1,0) [] []
    where
      coords = [(0,6), (0,3), (1,3), (1,0), (5,0), (5,4), (4,4), (4,6)]

