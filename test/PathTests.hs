{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module PathTests (pathTests) where

import Test.Tasty
import Test.Tasty.HUnit

import qualified Data.Map.Lazy as M
import qualified Data.Vector as V
import qualified Data.Sequence as Seq

import Types
import Utils
import Path

pathTests :: TestTree
pathTests = testGroup "Path tests"
  [ testCase "w2: prop (0,2)"  $
    makeStep w2 5 (M.singleton (0,2) 5) @?= M.fromList [ ((0,2),5),((0,1),6),((1,2),6) ]
  , testCaseSteps "w2: propagation" $ \step -> do
      step "(0,0)"
      let r0 = M.singleton (0,0) 0
      let r1 = M.insert (0,1) 1 r0
      makeStep w2 0 r0 @?= r1
      step "(0,1)"
      let r2 = M.insert (0,2) 2 r1
      makeStep w2 1 r1 @?= r2
      step "(0,2)"
      let r3 = M.insert (1,2) 3 r2
      makeStep w2 2 r2 @?= r3
      step "(1,2)"
      let r4 = M.insert (2,2) 4 r3
      makeStep w2 3 r3 @?= r4
      step "(2,2)"
      let r5 = M.insert (3,2) 5 $ M.insert (2,1) 5 r4
      makeStep w2 4 r4 @?= r5
  , testCase "w2: (0,0)-(2,2)" $
    pathBetween w2 (0,0) (2,2) @?= Just (Seq.fromList [u,u,r,r])
  , testCase "w2: (0,0)-(4,0)" $
    pathBetween w2 (0,0) (4,0) @?= Just (Seq.fromList [u,u,r,r,r,r,d,d])
  ]
  where u = MoveUp
        d = MoveDown
        l = MoveLeft
        r = MoveRight

w2 :: World
w2 = World
  { worldMap = V.generate h $ \y -> V.generate w $ \x -> if (x,y)`elem`stones
                                                         then (Stone,Nothing)
                                                         else (Empty,Nothing)
  , maxX = w
  , maxY = h
  , obstacles = []
  , spawner = Nothing
  , teleports = []
  }
  where stones = [(1,0),(1,1), (3,0),(3,1)]
        h = 3
        w = 5

