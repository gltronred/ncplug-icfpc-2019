{-# LANGUAGE OverloadedStrings #-}

module SearchTests (searchTests) where

import Test.Tasty
import Test.Tasty.HUnit

import Types
import Parser (parseProblem)
import Utils
import Search

searchTests :: TestTree
searchTests = testGroup "Search Tests"
  [ -- searchInExample
  -- , searchInExample2
  ]

searchInExample :: TestTree
searchInExample = testGroup "Search example"
  [ testCase "Really solved" $ countEmpty lastWorld @?= 0
  , testCase "All wrapped" $ countWrapped lastWorld @?= 15
  , testCase "Reasonable path length" $ length fullSolution > 5 @? "Path is too short to be true"
  ]
  where
    Right example = parseProblem "(0,0),(4,0),(4,4),(0,4)#(0,0)#(2,2),(3,2),(3,3),(2,3)#B(1,1);F(0,2)"
    Just fullSolution = shortestPath example
    lastWorld = world . last $ fullSolution

searchInExample2 :: TestTree
searchInExample2 = testGroup "Search example 2"
  [ testCase "Really solved" $ countEmpty lastWorld @?= 0
  , testCase "All wrapped" $ countWrapped lastWorld @?= 15
  , testCase "Reasonable path length" $ length fullSolution > 15 @? "Path is too short to be true"
  ]
  where
    Right example = parseProblem "(0,0),(10,0),(10,10),(0,10)#(0,0)#(4,2),(6,2),(6,7),(4,7);(5,8),(6,8),(6,9),(5,9)#B(0,1);B(1,1);F(0,2);F(1,2);L(0,3);X(0,9)"
    Just fullSolution = shortestPath example
    lastWorld = world . last $ fullSolution
    
