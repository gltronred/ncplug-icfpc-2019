#!/bin/bash

PRIVATE_ID=$1
dir=$2
now=$(date +%Y%m%d-%H%M%S)

pushd $dir
zip /tmp/solutions-$now prob-*.sol
popd

curl -F "private_id=${PRIVATE_ID}" -F "file=@/tmp/solutions-${now}.zip" https://monadic-lab.org/submit

