#!/bin/bash

soldir=$1
shift 1
servers=$@

ENABLE_LOCAL=0

REMOTE_ROOT=/tmp/ncplug
SRC="app package.yaml Setup.hs src stack.yaml test problems $soldir"

# for i in $servers; do
#     echo $i
# done

for server in $servers; do
    echo "Copy to $server"
    ssh $server mkdir -p "$REMOTE_ROOT/$soldir"
    rsync -azu $SRC $server:$REMOTE_ROOT/
    ssh $server rm -v "$REMOTE_ROOT/$soldir/*.sol"
    ssh $server "cd $REMOTE_ROOT; stack install --local-bin-path=./"
done

if [[ $ENABLE_LOCAL == 1 ]]; then
    echo "Copy to local machine..."
    mkdir -p $REMOTE_ROOT
    cp -r $SRC $REMOTE_ROOT/
    rm -v "$REMOTE_ROOT/$soldir/*.sol"
    pushd $REMOTE_ROOT
    stack install --local-bin-path=./
    popd
fi

