#!/bin/bash

soldir=$1
shift 1
servers=$@

ENABLE_LOCAL=1

REMOTE_ROOT=/tmp/ncplug

# for i in $servers; do
#     echo $i
# done

echo "Gather results..."
for server in $servers; do
    rsync -azu $server:$REMOTE_ROOT/$soldir/*.sol $soldir/
done
cp $REMOTE_ROOT/$soldir/*.sol $soldir/

