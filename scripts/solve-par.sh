#!/bin/bash

ENABLE_LOCAL=0

REMOTE_ROOT=/tmp/ncplug
JOBLOG=/tmp/joblog

solver=$1
algo=$2
probdir=$3
soldir=$4
ssh_logins=$5

shift 5

# echo "solver=$1 algo=$2 probdir=$3 soldir=$4"
# for i in $servers; do
#     echo $i
# done

# truncate -s0 $SSH_LOGINS
# for server in $servers; do
#     echo $server >> $SSH_LOGINS
# done
#
# if [[ $ENABLE_LOCAL == 1 ]]; then
#     echo : >> $SSH_LOGINS
# fi

echo "Working..."
parallel --will-cite --progress --eta --sshloginfile $ssh_logins -M --joblog $JOBLOG --workdir $REMOTE_ROOT ./$(basename $solver) $algo "$probdir/prob-{}.desc" "$soldir/prob-{}.sol" ::: $@

