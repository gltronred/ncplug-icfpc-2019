# NCPLUG entry for ICFPC 2019

We thank organizers for the contest. Organization was almost flawless and participation was fun! (Especially reading about Lambdacoin and its hard fork.)

We didn't make much progress. Here is what we have.

# Strategies

## A-star in the solution space

It simply runs A* on all possible moves through the space of solutions. Very slow, but theoretically should find the best answer.

## Heuristics 1: find path to the nearest empty cell

It also runs A*, but it looks for a path to the nearest empty cell

## Heuristics "Rects": divide and wrap

First, it divides the problem into empty rectangles (that do not contain obstacles). 

Second, it looks for the nearest rectangle with some empty cells using Lee algorithm.

Third, it wraps this rectangle in (almost) optimal way.

Then it repeats until there are no unwrapped cells left.

# Compile and run

## Compilation

Use stack to compile the program: `stack install --local-bin-path=./`.

* Source code for the program is in `src`, `app` and `test`. 
* Problem descriptions are in `problems`. 
* Helper scripts are in `scripts`.

## Run

You can run tests: `stack test`.

We also have several strategies and helper functions. To launch them use one of the following commands:

```
./ncplug2019    <problem.desc>                - tests reading description
./ncplug2019 a  <problem.desc> <output.sol>   - solves using A*
./ncplug2019 b  <problem.desc> <output.sol>   - solves using heuristics
./ncplug2019 c  <problem.desc> <output.sol>   - solves using rects
./ncplug2019 as <problem id>                  - solves using A* (shortcut)
./ncplug2019 bs <problem id>                  - solves using heuristics (shortcut)
./ncplug2019 d  <problem.desc> <output.sol>   - verifies solution file
./ncplug2019 rm <problem.desc> <output.sol>   - removes solution if invalid
./ncplug2019 f  <problem1.sol> <problem1.sol> - copies shorter file right-to-left
```

We recommend using algorithm b (works slower, but usually gives better results) or c (works much faster, even not close to optimal).

For example, to compute all solutions (except first) using algorithm b, use:

```
# Create directory for solutions
mkdir -p sol-b

# Solve first problem using A*
stack exec ncplug2019 a problems/prob-001.desc sol-b/prob-001.sol

# Solve all other problems using heuristics
for i in {002..220}; do
  stack exec ncplug2019 b problems/prob-$i.desc sol-b/prob-$i.sol
done

# Submit solutions
bash scripts/submit.sh <private_id> sol-b
```

## Parallel launch

There are scripts to launch solution of different problems in parallel on different machines.

We use `GNU parallel` to do such task and we have to cite it:
> Tange, Ole. GNU Parallel 2018 // Ole Tange, Mar 2018. ISBN 9781387509881. DOI: https://doi.org/10.5281/zenodo.1146014

It is assumed that solutions are collected to `sol` directory, problems are in `problems` (this can be changed). Job log is written to `/tmp/logjob` on local machine. All files are stored in `/tmp/ncplug`.

To prepare for launch we have to create `/tmp/ncplug` directory on all servers that contains `ncplug2019` executable alongside with `problems/` directory and empty `sol/` directory.
This is done using `prepare-sol.sh` (we suppose that each server is accesible by SSH using an ssh-key):

```
bash scripts/prepare-par.sh sol red@server.ru root@other.com root@third.org
```

Prepare `sshlogins` file in the following format (consult `man parallel`, section on `--sshlogin`):

```
red@server.ru
16/other.com
40/sshcmd -p 2222 root@third.org
```

It will run solution on three servers using maximum detected job count on first, 16 on second and 40 on third. It will also use `sshcmd -p 2222` instead of plain `ssh` to connect to third server.

Now you can launch parallel solving using `solve-par.sh`:

```
bash scripts/solve-par.sh ncplug2019 c problems sol sshlogins {001..050} 055 120
```

At any moment you can gather solutions that are ready:

```
bash scripts/gather-solutions.sh sol red@server.ru root@other.com root@third.org
```

