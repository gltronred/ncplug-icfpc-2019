{-# LANGUAGE RecordWildCards #-}

module Parser where

import Types

import qualified Data.Sequence as Seq
import Data.Text (Text)
import qualified Data.Text.IO as T
import Data.Void (Void)
import Text.Megaparsec
import Text.Megaparsec.Char

type Parser = Parsec Void Text

parseProblemFile :: FilePath -> IO (Either String Problem)
parseProblemFile f = parseProblem <$> T.readFile f

parseProblem :: Text -> Either String Problem
parseProblem s = case parse problemP "-" s of
  Left b -> Left $ errorBundlePretty b
  Right p -> Right p

parseSolution :: Text -> Either String Solution
parseSolution s = case parse solutionP "-" s of
  Left b -> Left $ errorBundlePretty b
  Right s -> Right s

parseSolutionFile :: FilePath -> IO (Either String Solution)
parseSolutionFile f = parseSolution <$> T.readFile f

problemP :: Parser Problem
problemP = do
  probMap <- polygonP
  char '#'
  probInit <- pointP
  char '#'
  probObstacles <- filter (not . null) <$> polygonP `sepBy` char ';'
  char '#'
  probBoosters <- boosterP `sepBy` char ';'
  pure Problem{..}

polygonP :: Parser Polygon
polygonP = pointP `sepBy` char ','

pointP :: Parser Pt
pointP = between (char '(') (char ')') $ do
  x <- natP
  char ','
  y <- natP
  pure (x,y)

natP :: Parser Nat
natP = read <$> some digitChar

negP :: Parser Int
negP = do
  char '-'
  n <- natP
  pure $ -n

relP :: Parser Int
relP = choice
  [ natP
    ,negP
  ]

boosterP :: Parser Booster
boosterP = do
  code <- boosterCodeP
  space
  pt <- pointP
  pure (code, pt)

boosterCodeP :: Parser BoosterCode
boosterCodeP = choice
  [ char 'B' *> pure Extension
  , char 'F' *> pure FastWheel
  , char 'L' *> pure Drill
  , char 'X' *> pure Mysterious
  , char 'R' *> pure Teleport
  , char 'C' *> pure Clone
  ]

solutionP :: Parser Solution
solutionP = Seq.fromList <$> many actionP

actionP :: Parser Action
actionP = choice
 [ char 'W' *> pure MoveUp
 , char 'S' *> pure MoveDown
 , char 'A' *> pure MoveLeft
 , char 'D' *> pure MoveRight
 , char 'Z' *> pure Nop
 , char 'E' *> pure TurnRight
 , char 'Q' *> pure TurnLeft
 , char 'F' *> pure StartFast
 , char 'L' *> pure StartDrill
 , char 'R' *> pure ResetTeleport
 , char 'C' *> pure StartClone
 , (\_ pt -> ShiftTeleport pt) <$> char 'T' <*> pointP
 , extendP
 ]

extendP = do
  char 'B'
  char '('
  x <- relP
  char ','
  y <- relP
  char ')'
  pure $ Extend (x,y)
