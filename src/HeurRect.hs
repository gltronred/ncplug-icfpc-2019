{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BangPatterns #-}

module HeurRect where

import Types
import Rules
import Utils
import Path

import Control.Monad
import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Writer.CPS
import Data.Graph.AStar
import Data.Function
import Data.Hashable
import qualified Data.HashMap.Strict as M
import Data.HashSet (HashSet, fromList)
import Data.Maybe
import Data.List
import qualified Data.Set as S
import Data.Set (Set)
import qualified Data.Sequence as Seq
import Data.Sequence (Seq,(><),(|>),empty)
import qualified Data.Vector as V
import GHC.Generics (Generic)
import System.Random


-- | First is the lower left corner, second is the upper right one
type Rect = (Pt,Pt)

data WS = WS
  { world :: !World
  , bot   :: !BotState
  , rects :: !(Set (Pt,Pt))
  } deriving (Eq,Show,Read)

insideRect :: Pt -> Rect -> Bool
insideRect (x,y) ((x0,y0),(xN,yN)) = x0 <= x && x <= xN && y0 <= y && y <= yN

ppRects :: WS -> [String]
ppRects WS{ world=World{..}, rects=rs } = map mkRow [0..maxY-1]
  where
    colors = "0123456789abdef"
    irs = zip (cycle colors) $ S.toList rs
    mkRow y = map (mkCell y) [0..maxX-1]
    mkCell y x = case find (\(i,r) -> insideRect (x,y) r) irs of
      Nothing -> ' '
      Just (i,_) -> i

solve :: Problem -> Maybe Solution
solve = go . start
  where
    turnR360 = replicate 4 TurnRight
    turnL360 = replicate 4 TurnLeft
    go :: WS -> Maybe Solution
    go ws
      | S.null (rects ws) = Just empty
      | otherwise = let
          w = world ws
          b = bot ws
          prs = mapMaybe (\r -> (r,) <$> waveToRect w r (pos b)) $ S.toList $ rects ws
          eprs = filter (emptyRect w.fst) prs
          -- (next,sp) = minimumBy (compare`on`(length.snd)) eprs
          (next,path) = minimumBy
            (\((bl1,tr1), p1) ((bl2,tr2), p2) ->
              compare (length p1 + dist ((bl1 `add` tr1) `sub` pos b)) (length p2 + dist ((bl2 `add` tr2) `sub` pos b)))
            eprs
          (w',b') = foldl' actionOutcome (w,b) path
          path' = wrapRectM b' next
          (!w'',b'') = foldl' actionOutcome (w',b') path'
          -- emptys = M.keys $ M.filter (==Empty) $ M.map fst $ worldMap w''
          -- r'' = S.fromList $ unfoldr containingRect (rects ws, emptys)
          -- r'' = S.filter (emptyRect w'') $ rects ws
          -- r'' = S.filter (/= next) $ rects ws
          -- r'' = S.delete next $ rects ws
          r'' = S.fromList $ delete next $ map fst eprs
          p = path >< path'
          in if null eprs
             then Just empty
             else (p><) <$> go (WS { world=w'', bot=b'', rects=r''})

-- | Initial state
start :: Problem -> WS
start p = WS { world = initialWorld, bot = initialBot, rects = initialRects }
  where
    initialWorld = problemToWorld p
    initialBot   = problemToBotState p
    initialRects = worldToRects initialWorld

emptyRect :: World -> Rect -> Bool
emptyRect World{ worldMap=w } ((x0,y0),(xN,yN)) = any emptyCol [x0..xN]
  where
    emptyCol x = any (emptyCell . (w !@) . (x,)) [y0..yN]
    emptyCell (Empty, _) = True
    emptyCell _ = False

worldToRects :: World -> Set Rect
worldToRects w = let
  seeds = [1..25]
  kvs = assocs $ worldMap w
  -- idxs = V.imap (\y row -> V.map (y,) $ V.findIndices ((/=Stone).fst) row) $ worldMap w
  -- m = S.fromList $ concatMap V.toList $ V.toList idxs
  m = S.fromList $ map fst $ filter ((/=Stone).fst.snd) kvs
  gs = map mkStdGen seeds
  in minimumBy (compare`on`S.size) $ map (mapToRects (maxX w,maxY w) m) gs

mapToRects :: Pt -> Set Pt -> StdGen -> Set Rect
mapToRects (maxX,maxY) w gen = S.fromList $ unfoldr mkRect (w,gen)
  where
    mkRect (m,g)
      | S.null m = Nothing
      | otherwise = Just $ let
          (rnd1,g') = next g
          (rnd2,g'') = next g'
          x = abs rnd1 `mod` maxX
          y = abs rnd2 `mod` maxY
          c = fromMaybe (S.findMin m) $ S.lookupLT (x,y) m
          r = extendRect (c,c) m
          m'= removeRect r m
          in (r,(m',g''))

extendRect :: Rect -> Set Pt -> Rect
extendRect r m = last $ r : unfoldr f r
  where
    f c = let
      u = fromMaybe c $ extendRectDir (\((l,d),(r,u)) -> [ (x,u+1) | x <- [l..r] ]) c m
      l = fromMaybe u $ extendRectDir (\((l,d),(r,u)) -> [ (l-1,y) | y <- [d..u] ]) u m
      d = fromMaybe l $ extendRectDir (\((l,d),(r,u)) -> [ (x,d-1) | x <- [l..r] ]) l m
      r = fromMaybe d $ extendRectDir (\((l,d),(r,u)) -> [ (r+1,y) | y <- [d..u] ]) d m
      in if c==r then Nothing else Just (r,r)

extendRectDir :: (Rect -> [Pt]) -> Rect -> Set Pt -> Maybe Rect
extendRectDir mkPts rect m = let
  pts = mkPts rect
  in if all (`S.member`m) pts
     then Just $ addPts pts rect
     else Nothing

addPts :: [Pt] -> Rect -> Rect
addPts ps (ul,dr) = bbox
  where pts = ul : dr : ps
        xs = map fst pts
        ys = map snd pts
        bbox = ((minimum xs, minimum ys), (maximum xs, maximum ys))

-- TODO: change list comprehension to something fusable
removeRect :: Rect -> Set Pt -> Set Pt
removeRect (tl,dr) m = m `S.difference` rectCells
  where rectCells = S.fromList [ (x,y)
                               | x <- [fst tl .. fst dr]
                               , y <- [snd tl .. snd dr]
                               ]

waveToRect :: World -> Rect -> Pt -> Maybe Solution
waveToRect w (a,_) p = pathBetween w p a

data HeurState = HS { heurBot :: !BotState, heurSol :: !Solution }

type Heur = State HeurState

runHeur :: BotState -> Heur a -> (a, HeurState)
runHeur bs = flip runState (HS bs empty)

emitMove :: Action -> Heur ()
emitMove move = modify' $ \(HS bs sol) -> HS (simulateStep bs move) (sol |> move)

wrapRectM :: BotState -> Rect -> Solution
wrapRectM bs rect@((x0,y0),(xN,yN)) = heurSol $ snd $ runHeur bs $ do
  moveBot (x0,y0)
  case (xN-x0, yN-y0) of
    (0,0)     -> return () -- 1x1 rect, need nothing to do more
    (x,y)
      | x < 2 &&
        y < 2 -> turnBot360
    _         -> wrapAll rect

wrapAll :: Rect -> Heur ()
wrapAll ((x0,y0),(xN,yN)) = forM_ [y0, y0+4 .. yN] $ \y -> do
  moveBot (xN,y)
  if
    | y+3 < yN -> do -- 4 or more rows left, business as usual
      emitMove MoveUp
      emitMove MoveUp
      turnBot180Right
      moveBot (x0,y+2)
      emitMove MoveUp
      emitMove MoveUp
      turnBot180Right
    | y+2 < yN -> do -- only 3 rows left, this is the last iteration
      emitMove MoveUp
      emitMove MoveUp
      turnBot180Right
      moveBot (x0,y+2)
      emitMove MoveUp
      turnBot180Right
      moveBot (xN,y+3) -- that would be the last row and loop won't cont it
    | (y+1 < yN) -> do -- only 2 rows left, this is the last iteration
      emitMove MoveUp
      moveBot (x0,y+1)
      turnBot360
    | otherwise -> do
      moveBot (x0,y)
      turnBot360

moveBot :: Pt -> Heur ()
moveBot (tx,ty) = do
  (sx,sy) <- gets $ pos . heurBot
  let rep n a
        | n < 0 = replicateM_ (-n) $ emitMove $ switchDir a
        | n ==0 = pure ()
        | otherwise = replicateM_ n $ emitMove a
  rep (tx-sx) MoveRight
  rep (ty-sy) MoveUp

turnBot360 :: Heur ()
turnBot360 = replicateM_ 4 $ emitMove TurnLeft

turnBot180Right :: Heur ()
turnBot180Right = emitMove TurnRight >> emitMove TurnRight

turnBotRightAndBack :: Heur ()
turnBotRightAndBack = emitMove TurnRight >> emitMove TurnLeft

-- | Precondition: taking Action produces valid state
simulateStep :: BotState -> Action -> BotState
simulateStep bs@BS{ pos = (xb, yb), fastFor = ff } = \case
  MoveRight -> bs{ pos = (xb + step, yb), fastFor = max 0 $ ff-1 }
  MoveLeft  -> bs{ pos = (xb - step, yb), fastFor = max 0 $ ff-1 }
  MoveUp    -> bs{ pos = (xb, yb + step), fastFor = max 0 $ ff-1 }
  MoveDown  -> bs{ pos = (xb, yb - step), fastFor = max 0 $ ff-1 }
  _         -> bs{ fastFor = max 0 $ ff-1 }
  where
    step = if ff > 0 then 2 else 1

switchDir :: Action -> Action
switchDir MoveRight = MoveLeft
switchDir MoveLeft  = MoveRight
switchDir MoveUp    = MoveDown
switchDir MoveDown  = MoveUp


