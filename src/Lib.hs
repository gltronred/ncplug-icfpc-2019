module Lib where

import Prelude hiding (log)

import Types
import Parser (parseProblemFile, parseProblem, parseSolutionFile)

import qualified Search
import qualified Heur1
import qualified HeurRect

import qualified Data.Text.IO as T
import System.Environment
import System.Exit
import System.IO

import Control.Exception(try,displayException, SomeException, Exception, throw)
import Utils
import Rules
import Text.Printf(printf)
import System.Directory
import Control.Monad(when)
import Data.Maybe(isNothing)

log :: String -> IO ()
log = hPutStrLn stderr

withIOFiles :: FilePath -> FilePath -> (Problem -> Maybe Solution) -> IO ()
withIOFiles fin fout solve = do
  ep <- parseProblemFile fin
  case ep of
    Left e -> die $ fin ++ ": FAIL: " ++ e
    Right p -> do
      let msol = solve p
      withFile fout WriteMode $ \hout ->
        case msol of
          Nothing -> die $ fin ++ ": FAIL"
          Just sol -> T.hPutStrLn hout $ ppSolution sol

ncplugMain :: IO ()
ncplugMain = do
  args <- getArgs
  case args of
    [f] -> do
      ep <- parseProblemFile f
      case ep of
        Left e -> log $ f ++ ": FAIL: " ++ e
        Right p -> log $ f ++ ": OK"
    ["as", pid] -> do
      let fin  = "problems/prob-" ++ pid ++ ".desc"
          fout = "solutions/prob-" ++ pid ++ ".sol"
      withIOFiles fin fout Search.solve
    ["bs", pid] -> do
      let fin  = "problems/prob-" ++ pid ++ ".desc"
          fout = "solutions/prob-" ++ pid ++ ".sol"
      withIOFiles fin fout Heur1.solve
    ["a", fin, fout] -> do
      withIOFiles fin fout Search.solve
    ["b", fin, fout] -> do
      withIOFiles fin fout Heur1.solve
    ["c", fin, fout] -> do
      withIOFiles fin fout HeurRect.solve
    ["d", ftask, fsol] -> do
      v <- validateSolutionFile ftask fsol
      putStrLn $ if v then "Ok" else "Failed"
    ["e", fsol] -> do
      l <- solutionFileLength fsol
      putStrLn $ show l
    ["rm", ftask, fsol] -> do
      v <- validateSolutionFile ftask fsol
      if v then putStrLn "Ok"
        else removeFile fsol >> putStrLn "Failed"
    ["f", fsol1, fsol2] -> do
       l1 <- solutionFileLength fsol1
       l2 <- solutionFileLength fsol2
       if l2 < l1 then do
           copyFile fsol2 fsol1
           putStrLn $ (show l1) ++ " <-- " ++ (show l2)
         else putStrLn $ (show l1) ++ " / " ++ (show l2)
    ["va", dprob, dsol] -> do
       r <- validateSolutions False dprob dsol
       putStrLn $ unlines $ map show r
    ["vr", dprob, dsol] -> do
       r <- validateSolutions True dprob dsol
       putStrLn $ unlines $ map show r
    ["min", dprob, dsol1, dsol2] -> do
        mergeSolutions dprob dsol1 dsol2
    _ -> do
      putStrLn "Usage: "
      putStrLn "  ncplug2019    <problem.desc>                - tests reading description"
      putStrLn "  ncplug2019 a  <problem.desc> <output.sol>   - solves using A*"
      putStrLn "  ncplug2019 b  <problem.desc> <output.sol>   - solves using heuristics"
      putStrLn "  ncplug2019 c  <problem.desc> <output.sol>   - solves using rects"
      putStrLn "  ncplug2019 as <problem id>                  - solves using A* (shortcut)"
      putStrLn "  ncplug2019 bs <problem id>                  - solves using heuristics (shortcut)"
      putStrLn "  ncplug2019 d  <problem.desc> <output.sol>   - verifies solution file"
      putStrLn "  ncplug2019 rm <problem.desc> <output.sol>   - removes solution if invalid"
      putStrLn "  ncplug2019 f  <problem1.sol> <problem1.sol> - copies shorter file right-to-left"




-- calcRemotely :: String -> FilePath -> FilePath -> (Problem -> Maybe Solution) -> IO Bool


calcMissing :: FilePath -> FilePath -> (Problem -> Maybe Solution) -> IO [Int]
calcMissing p s solver = calcMissing' p s solver 0

calcMissing' :: FilePath -> FilePath -> (Problem -> Maybe Solution) -> Int -> IO [Int]
calcMissing' p s solver n = do
  let num = printf "%03d" n
  let probF = p++"/prob-"++num++".desc"
  let solF = s++"/prob-"++num++".sol"
  existP <- doesFileExist probF
  existS <- doesFileExist solF
  if not (existP) then return []
    else if existS then calcMissing' p s solver (n+1)
      else do
        putStr $ show n
        withIOFiles probF solF solver
        putStrLn "===>|"
        r <- calcMissing' p s solver (n+1)
        return (n:r)

mergeSolutions :: FilePath -> FilePath -> FilePath -> IO ()
mergeSolutions probD sol1D sol2D = mergeSolutions' probD sol1D sol2D 1

mergeSolutions' :: FilePath -> FilePath -> FilePath -> Int -> IO ()
mergeSolutions' probD sol1D sol2D n = do
  let num = printf "%03d" n
  let probF = probD++"/prob-"++num++".desc"
  let sol1F = sol1D++"/prob-"++num++".sol"
  let sol2F = sol2D++"/prob-"++num++".sol"
  putStrLn " "
  putStr num
  exist <- doesFileExist probF
  if not exist then return ()
    else do
        exist1 <- doesFileExist sol1F
        exist2 <- doesFileExist sol2F
        if not exist2 then mergeSolutions' probD sol1D sol2D (n+1)
          else if not exist1 then do
              putStrLn " <="
              copyFile sol2F sol1F
            else do
              l1 <- solutionFileLength sol1F
              l2 <- solutionFileLength sol2F
              if l2 < l1 then do
                copyFile sol2F sol1F
                putStrLn $ (show l1) ++ " <-- " ++ (show l2)
                mergeSolutions'  probD sol1D sol2D (n+1)
              else do
                putStrLn $ (show l1) ++ " / " ++ (show l2)
                mergeSolutions'  probD sol1D sol2D (n+1)

validateSolutions :: Bool -> FilePath -> FilePath -> IO [(Int, Maybe Int)]
validateSolutions erase probD solD = validateSolutionSteps erase probD solD 1

validateSolutionSteps :: Bool -> FilePath -> FilePath -> Int -> IO [(Int,Maybe Int)]
validateSolutionSteps erase probD solD n = do
  let num = printf "%03d" n
  let probF = probD++"/prob-"++num++".desc"
  exist <- doesFileExist probF
  if not (exist) then return []
    else do
      r <- validateSolutionNum erase probD solD n
      rs <- validateSolutionSteps erase probD solD (n+1)
      return ((n,r):rs)

takeWhileM :: (Monad m) => (a -> m Bool) -> [a] -> m [a]
takeWhileM _ [] = return []
takeWhileM p (x:xs) = do
  bool <- p x
  if bool
    then do
      rs <- takeWhileM p xs
      return (x:rs)
    else return []

validateSolutionNum :: Bool ->FilePath -> FilePath -> Int -> IO (Maybe Int)
validateSolutionNum erase probD solD n = do
  let num = printf "%03d" n
  let probF = probD++"/prob-"++num++".desc"
  let solF = solD++"/prob-"++num++".sol"
  r <- validateSolutionNum' probD solD n
  when (isNothing r) $ do
    e <- doesFileExist solF
    when e $ removeFile solF
  return r

validateSolutionNum' :: FilePath -> FilePath -> Int -> IO (Maybe Int)
validateSolutionNum' probD solD n = do
  let num = printf "%03d" n
  let probF = probD++"/prob-"++num++".desc"
  let solF = solD++"/prob-"++num++".sol"
  (Right p) <- parseProblemFile probF
  exist <- doesFileExist solF
  sol <- try $ parseSolutionFile solF :: IO (Either SomeException (Either String Solution))
  putStr $ num ++ "\t"
  if not exist then putStrLn "NO" >> return Nothing
  else case sol of
    Left e -> putStrLn ("BAD\t\t" ++ (displayException e)) >> return Nothing
    Right (Left e) -> putStrLn ("BAD\t\t" ++ (show e)) >> return Nothing
    Right (Right s) -> do
      v <- validateSolution p s
      if v then putStrLn ("GOOD\t"++ (show (length s))) >> return (Just (length s))
        else putStrLn "INV" >> return Nothing

validateSolutionFile :: FilePath -> FilePath -> IO Bool
validateSolutionFile prob sol = do
  (Right p) <- parseProblemFile prob
  (Right s) <- parseSolutionFile sol
  validateSolution p s

validateSolution :: Problem -> Solution -> IO Bool
validateSolution p s = do
  let w = problemToWorld p
  let b = problemToBotState p
  finWorldOk <- (try $ return $ countEmpty $ fst $ run (w,b) s) :: IO (Either SomeException Int)
  case finWorldOk of
    Left e -> do
      putStrLn $ "evaluation error: " ++ (displayException e)
      return False
    Right num -> if(num/=0)
      then do
        putStrLn $ "unpainted cells:" ++ (show num)
        return False
      else return True

data MyException = MoveNotAllowed String
    deriving Show

instance Exception MyException

run :: (World,BotState) -> Solution -> (World,BotState)
run = foldl (\(wn,bn) a -> if (actionAllowed wn bn a) then act a (wn,bn) else throw $ MoveNotAllowed $ show a)

solutionFileLength :: FilePath -> IO Int
solutionFileLength sol = do
  (Right s) <- parseSolutionFile sol
  return $ length s

mergeSolution :: FilePath -> FilePath -> IO Int
mergeSolution dest src = do
    (Right d) <- parseSolutionFile dest
    (Right s) <- parseSolutionFile src
    if (length d) > length s
      then copyFile src dest >> (return $ length s)
      else return $ length d

submitSolutions :: String -> FilePath -> IO (Int,String)
submitSolutions = undefined

