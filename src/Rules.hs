{-# LANGUAGE RecordWildCards #-}

module Rules where

import Types
import Utils

import Control.Monad (join)
import Data.Function (on)
import Data.List(minimumBy, nub, delete, unfoldr, sort)
import Data.Maybe
import Data.Traversable (traverse)
import Data.Vector (Vector,(!),(//))
import qualified Data.Vector as V

line :: Pt -> Pt -> [Pt]
line pa@(xa,ya) pb@(xb,yb) = map maySwitch . unfoldr go $ (x1,y1,0)
  where
    steep = abs (yb - ya) > abs (xb - xa)
    maySwitch = if steep then (\(x,y) -> (y,x)) else id
    [(x1,y1),(x2,y2)] = sort [maySwitch pa, maySwitch pb]
    deltax = x2 - x1
    deltay = abs (y2 - y1)
    ystep = if y1 < y2 then 1 else -1
    go (xTemp, yTemp, error)
        | xTemp > x2 = Nothing
        | otherwise  = Just ((xTemp, yTemp), (xTemp + 1, newY, newError))
        where
          tempError = error + deltay
          (newY, newError) = if (2*tempError) >= deltax
                            then (yTemp+ystep,tempError-deltax)
                            else (yTemp,tempError)
inMap :: World -> Pt -> Bool
inMap w (x,y) = not $ (x < 0) || (x >= maxX w) || (y < 0) || (y >= maxY w)

inField :: World -> Pt -> Bool
inField w pos
  | not $ inMap w pos = False
  | otherwise = fst (worldMap w !@ pos) /= Stone

isVisible :: World -> Pt -> Pt -> Bool
isVisible w src tgt = let
  pixels = line src tgt
--  fields = map fst $ map (\ pix -> worldMap w ! pix ) pixels
    in all (inField w) pixels

obstacleOn :: World -> Pt -> Bool
obstacleOn w pos
  | not $ inMap w pos = True
  | otherwise = fst (worldMap w !@ pos) == Stone

moveDir :: Action -> RelPt
moveDir a = case a of
  MoveUp    -> (0,1)
  MoveDown  -> (0,-1)
  MoveLeft  -> (-1,0)
  MoveRight -> (1,0)
  _         -> (0,0)

isPositionteTeportable :: World -> Pt -> Bool
isPositionteTeportable w pt = pt `notElem` teleports w && (spawner w /= Just pt)

actionAllowed :: World -> BotState -> Action -> Bool
actionAllowed w bot step = let
  fast = fastFor bot > 0
  drilling = drillingFor bot > 0
  dir = moveDir step
  nextPos' = add (pos bot)
--  nextPos d = if fast then [add (nextPos' d) d, nextPos' d] else [nextPos' d]
  newPos = nextPos' dir -- fast bot is allowed to skip second step
  allowed dir = (inField w newPos) && (drilling || not (obstacleOn w newPos))
  adjacent (newX,newY) = any (\(x,y)-> (x==newX && (y==newY+1 || y== newY-1)) || (y==newY && (x==newX-1 || x ==newX+1)))
    in case step of
      Nop   -> True
      TurnRight -> True
      TurnLeft -> True
      Extend rel -> (elem Extension $ boosters bot) && (adjacent rel (hands bot))
      StartFast -> elem FastWheel $ boosters bot
      StartDrill -> elem Drill $ boosters bot
      MoveUp -> allowed (0,1)
      MoveDown -> allowed (0,-1)
      MoveLeft -> allowed (-1,0)
      MoveRight -> allowed (1,0)
      ResetTeleport -> (elem Teleport (boosters bot)) && (isPositionteTeportable w (pos bot))
      ShiftTeleport tpos -> elem tpos $ teleports w
      StartClone -> (elem Clone (boosters bot)) && isJust (spawner w)

handsRight :: [RelPt] -> [RelPt]
handsRight = map (\(x,y )-> (y,-x))

handsLeft ::  [RelPt] -> [RelPt]
handsLeft = map (\(x,y) -> (-y,x))

handsExtend :: RelPt -> [RelPt] -> [RelPt]
handsExtend new = (new:)

type Change = (World,BotState) -> (World, BotState)

boostsTick :: Change
boostsTick (w,b) = let
  drill = max 0 $ drillingFor b - 1
  fast = max 0 $ fastFor b - 1
    in (w,b{drillingFor = drill, fastFor = fast})

boosterPick :: Pt -> Change
boosterPick pos@(x,y) (w,b) = let
  oldMap = worldMap w
  (oldC,picked) =  oldMap !@ pos
    in maybe (w,b) (\boost ->
        if boost == Mysterious then (w,b) else
          (w{worldMap = updateAt oldMap pos (oldC,Nothing)}, b{boosters = boost: (boosters b)})
      ) picked

wrapAt :: World -> Pt -> [RelPt] -> [Pt]
wrapAt w center hands = let
  handPos = map (add center) hands
  visHands = filter (isVisible w center) handPos
      in filter (not . obstacleOn w) visHands

wrapCell :: WorldMap (Cell, Maybe BoosterCode) -> Pt -> WorldMap (Cell, Maybe BoosterCode)
wrapCell amap pos = adjust (\(c,b) -> (Wrapped,b)) pos amap

wrapCells :: [Pt] -> WorldMap (Cell, Maybe BoosterCode) -> WorldMap (Cell, Maybe BoosterCode)
wrapCells pts amap = foldl wrapCell amap pts

applyWrap :: Pt -> [RelPt] -> Change
applyWrap center hands (w,bot) = (w{worldMap = wrapCells (wrapAt w center hands) (worldMap w)},bot)

wrap :: Change
wrap (w,bot) = let
  p = pos bot
  h = hands bot
    in applyWrap p h (w,bot)

drillCell :: Pt -> World -> World
drillCell pos w = w{worldMap = adjust (\(c,b) -> (Empty,b)) pos (worldMap w)}

moveSlow :: Pt -> Change
moveSlow newPos (w,bot) = let
  isDrilling = drillingFor bot > 0
  drill (w',b') | isDrilling = (drillCell newPos w',b')
    | otherwise = (w',b')
    in wrap $ drill $ boosterPick newPos (w,bot{pos = newPos})

move' :: RelPt -> Change
move' dir (w, bot) = let
  oldPos = pos bot
  newPos = add oldPos dir
    in moveSlow newPos (w,bot)

move :: RelPt -> Change
move dir (w, bot) = let
  isFast = fastFor bot > 0
  isDrilling = drillingFor bot > 0
  step1 = move' dir (w,bot)
  pos2 = add (pos (snd step1)) dir
    in if isFast && ((inField w pos2) || (isDrilling && inMap w pos2))
        then move' dir step1
        else step1

installTeleport:: Change
installTeleport (w,bot) = (w{teleports= (pos bot):(teleports w)},bot{boosters= delete Teleport (boosters bot)})

act' :: Action -> Change
act' step (w,bot) = let
  oldPos = pos bot
  oldHands = hands bot
  oldBoosters = boosters bot
    in case step of
      Nop        -> (w,bot)
      TurnRight  -> let newHands = handsRight oldHands
        in applyWrap oldPos newHands (w, bot{ hands = newHands })
      TurnLeft   -> let newHands = handsLeft oldHands
        in applyWrap oldPos newHands (w, bot{ hands = newHands })
      Extend rel -> let newHands = handsExtend rel oldHands
                        newBoosters = delete Extension oldBoosters
        in applyWrap oldPos newHands (w, bot{ hands = newHands, boosters = newBoosters})
      StartFast  -> let newBoosters = delete FastWheel oldBoosters
        in (w, bot{ fastFor = fastFor bot + 50, boosters = newBoosters })
      StartDrill -> let newBoosters = delete Drill oldBoosters
        in (w, bot{ drillingFor = drillingFor bot + 30, boosters = newBoosters })
      MoveUp     -> move (moveDir step) (w,bot)
      MoveDown   -> move (moveDir step) (w,bot)
      MoveLeft   -> move (moveDir step) (w,bot)
      MoveRight  -> move (moveDir step) (w,bot)
      ResetTeleport     -> installTeleport (w,bot)
      ShiftTeleport pos -> moveSlow pos (w,bot)
      StartClone -> (w,bot) -- FIXME

act :: Action -> Change
act s wb = boostsTick $ act' s wb

actionOutcome = flip act
