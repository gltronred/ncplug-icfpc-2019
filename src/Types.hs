{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE DeriveGeneric #-}

module Types where

import Data.Hashable
import Data.HashMap.Strict (HashMap)
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Data.Aeson
import qualified Data.ByteString.Lazy as BS
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import qualified Data.Foldable as F
import Data.Vector (Vector)

type Nat = Int

type Pt = (Nat, Nat)
type RelPt = (Int, Int)

-- Список углов в порядке обхода против часовой стрелки
type Polygon = [Pt]

data BoosterCode
  = Extension  -- B
  | FastWheel  -- F
  | Drill      -- D
  | Teleport   -- R
  | Mysterious -- X
  | Clone      -- C
  deriving (Eq,Ord,Show,Read,Generic)

instance Hashable BoosterCode

type Booster = (BoosterCode, Pt)

data Problem = Problem
  { probMap       :: Polygon
  , probInit      :: Pt
  , probObstacles :: [Polygon]
  , probBoosters  :: [Booster]
  } deriving (Eq,Show,Read)


data Action
  = MoveUp                      -- W
  | MoveDown                    -- S
  | MoveLeft                    -- A
  | MoveRight                   -- D
  | Nop                         -- Z
  | TurnRight                   -- E
  | TurnLeft                    -- Q
  | Extend RelPt                -- B (dx,dy)
  | StartFast                   -- F; 50 units of time
  | StartDrill                  -- L; 30 units of time
  | ResetTeleport               -- R
  | ShiftTeleport Pt            -- T
  | StartClone                  -- C
  deriving (Eq,Ord,Show,Read,Generic)

instance Hashable Action

type Solution = Seq Action
type Solutions = [Solution]

ppAction :: Action -> Text
ppAction = \case
  MoveUp            -> "W"
  MoveDown          -> "S"
  MoveLeft          -> "A"
  MoveRight         -> "D"
  Nop               -> "Z"
  TurnRight         -> "E"
  TurnLeft          -> "Q"
  Extend pos        -> "B" <> T.pack (show pos)
  StartFast         -> "F"
  StartDrill        -> "L"
  ResetTeleport     -> "R"
  ShiftTeleport pos -> "T" <> T.pack (show pos)
  StartClone        -> "C"

ppSolution :: Solution -> Text
ppSolution = T.concat . map ppAction . F.toList

ppSolutions :: Solutions -> Text
ppSolutions = T.intercalate "#" . map ppSolution

type Duration = Nat
data BotState
  = BS
  { pos         :: Pt
  , hands       :: [RelPt]
  , fastFor     :: Int
  , drillingFor :: Int
  , boosters    :: [BoosterCode]
  } deriving (Eq,Ord,Read,Show,Generic)

instance Hashable BotState

startHands :: [RelPt]
startHands = [(1,0),(1,1),(1,-1),(0,0)]

problemToBotState :: Problem -> BotState
problemToBotState p = BS { pos = probInit p, hands = startHands, fastFor = 0, drillingFor = 0, boosters = []}

data Cell
  = Empty
  | Wrapped
  | Stone
  deriving (Eq,Ord,Show,Read,Generic)

instance Hashable Cell

type WorldMap a = Vector (Vector a)

data World
  = World
  { worldMap  :: WorldMap (Cell, Maybe BoosterCode)
  , maxX      :: Nat
  , maxY      :: Nat
  , spawner   :: Maybe Pt
  , obstacles :: [Polygon]
  , teleports :: [Pt]
  } deriving (Eq,Ord,Read,Show,Generic)

-- instance Hashable World

data Puzzle
  = Puzzle
  { bNum :: Int
  , eNum :: Int
  , tSize:: Int
  , vMin :: Int
  , vMax :: Int
  , mNum :: Int
  , fNum :: Int
  , dNum :: Int
  , rNum :: Int
  , cNum :: Int
  , xNum :: Int
  , iSqs :: [Pt]
  , oSqs :: [Pt]
  } deriving (Eq,Ord, Read, Show, Generic)

data Block
  = Block
  { blockNum   :: Int
  , block_subs :: Int
  , block_ts   :: Float
  , excluded   :: [String]
  , balances   :: [(String,Int)]
  , puzzle     :: Puzzle
  , task       :: Problem
  }

instance ToJSON World where
  toEncoding = genericToEncoding defaultOptions

instance FromJSON World

instance ToJSON BoosterCode where
  toEncoding = genericToEncoding defaultOptions

instance FromJSON BoosterCode

instance ToJSON Cell where
  toEncoding = genericToEncoding defaultOptions

instance FromJSON Cell

