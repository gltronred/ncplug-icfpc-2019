{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module Search
  ( SearchState(..)
  , shortestPath
  , solve ) where

import Data.Graph.AStar
import Data.Hashable
import Data.HashSet (HashSet, fromList)
import GHC.Generics (Generic)

import Debug.Trace

import Types
import Rules
import Utils


data SearchState
  = SearchState
  { world    :: World
  , botState :: BotState
  , action   :: Action
  } deriving (Eq, Ord, Show, Generic)

-- instance Hashable SearchState

-- | Initial state
start :: Problem -> SearchState
start p = SearchState { world = initialWorld, botState = initialBot, action = Nop }
  where
    initialWorld = problemToWorld p
    initialBot   = problemToBotState p

-- | Whether we've wrapped all free space (search goal)
allWrapped :: SearchState -> Bool
allWrapped = (== 0) . countEmpty . world

-- | Distance between *neighbour* states
distance :: SearchState -> SearchState -> Int
distance s1 s2 = h1 - abs (h1 - h2) - abs (e1 - e2)
  where
    e1 = countEmpty $ world s1
    e2 = countEmpty $ world s2
    h1 = handCount s1
    h2 = handCount s2

-- | Distance to the goal (approximate, never overshoot)
heuristic :: SearchState -> Int
heuristic s = (`div` handCount s) $ countEmpty $ world s

-- nextState :: SearchState -> HashSet SearchState
-- nextState SearchState{..} = fromList $ fmap nextState' allowed
--   where
--     nextState' a = let (w, b) = act a (world, botState) in SearchState{ world = w, botState = b, action = a }
--     allowed = filter (actionAllowed world botState) actions
--     actions =
--       [ MoveUp
--       , MoveDown
--       , MoveLeft
--       , MoveRight
--       -- , Nop -- that looks stupid
--       , TurnRight
--       , TurnLeft
--       -- , Extend RelPt -- TODO
--       , StartFast
--       , StartDrill
--       ]

shortestPath :: Problem -> Maybe [SearchState]
shortestPath = error "Reimplement me!"
  -- aStar nextState distance heuristic allWrapped . start

solve :: Problem -> Maybe Solution
solve = error "Reimplement me!"
  -- fmap (fmap action) . shortestPath

handCount :: SearchState -> Int
handCount s = (+1) $ length $ hands $ botState s -- (+1) is for body
