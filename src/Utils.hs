{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TupleSections #-}

module Utils where

import Types

import Data.Char
import Data.Function
import Data.List
import Data.Maybe (fromMaybe, listToMaybe)
import qualified Data.HashMap.Strict as M
import qualified Data.Set as S
import Data.Tuple
import qualified Data.Vector as V
import Data.Vector (Vector, (!), (//))

(!@) :: Vector (Vector a) -> Pt -> a
(!@) w (x,y) = w ! y ! x

updateAt :: Vector (Vector a) -> Pt -> a -> Vector (Vector a)
updateAt w (x,y) a = w // [(y, (w!y) // [(x,a)])]

adjust :: (a -> a) -> Pt -> WorldMap a -> WorldMap a
adjust f p w = let a = w!@p in updateAt w p $ f a

assocs :: WorldMap a -> [(Pt,a)]
assocs = concat . V.toList . V.imap (\y row -> map (\(x,a) -> ((x,y),a)) $ V.toList $ V.imap (,) row)

add :: Pt -> RelPt -> Pt
add (x,y) (dx,dy) = (x+dx, y+dy)

sub :: Pt -> Pt -> RelPt
sub (x1,y1) (x2,y2) = (x1-x2, y1-y2)

dist :: Pt -> Int
dist (x,y) = abs x + abs y

insidePoly :: Pt -> Polygon -> Bool
insidePoly p poly = let
  edges = zip poly $ tail poly ++ [head poly]
  in odd $ length $ filter id $ map (intersectInfPt p) edges

intersectInfPt :: Pt -> (Pt,Pt) -> Bool
intersectInfPt (x,y) (p1,p2)
  | snd (p2`sub`p1) == 0 = False
  | otherwise = fst p1 <= x && minY <= y && y < maxY
  where
    y1 = snd p1
    y2 = snd p2
    minY = y1`min`y2
    maxY = y1`max`y2

leftTo :: Pt -> (Pt,Pt) -> Bool
leftTo p (b,a)
  | dx == 0 = (py>=0) == (dy>=0)
  | dy == 0 = (px>=0) == (dx>=0)
  | otherwise = error "Uh-oh! Line is not parallel to the axis"
  where (dx,dy) = b`sub`a
        (px,py) = p`sub`a

-- Нужно учитывать, что для ячейки координаты центра 0.5,0.5
-- Первый параметр - индекс ячейки
-- Возвращается удвоенное косое произведение
skewMult :: Pt -> (Pt,Pt) -> Int
skewMult p@(x,y) (a,b) = let
  (dx1,dy1) = p`sub`a
  (dx2,dy2) = b`sub`a
  in 2*dx1*dy2 - 2*dx2*dy1 + dy2 - dx2

problemToWorld :: Problem -> World
problemToWorld Problem{..}
  = World {..}
  where obstacles = probObstacles
        maxX = maximum $ map fst probMap
        maxY = maximum $ map snd probMap
        initialWrapped = [ (ix+1,iy), (ix+1,iy+1), (ix+1, iy-1), (ix,iy) ]
        (ix,iy) = probInit
        boosters = map swap probBoosters
--        boosters = filter (\(p,bc)-> bc /= Mysterious) boosters'
        spawner = listToMaybe $ map fst $ filter (\(p,bc)-> bc == Mysterious) boosters
        teleports = []
        worldMap =
          V.generate maxY $ \y ->
          V.generate maxX $ \x ->
          let
            p = (x,y)
            c | any (insidePoly p) probObstacles ||
                not (insidePoly p probMap) = Stone
              | p `elem` initialWrapped    = Wrapped
              | otherwise                  = Empty
            mb = lookup p boosters
          in (c,mb)

ppWorld :: World -> [String]
ppWorld = map (map toChar) .
          V.toList .
          V.map V.toList .
          worldMap
  where toChar (Stone,_) = '#'
        toChar (Empty,Nothing) = ' '
        toChar (Wrapped,Nothing) = '.'
        toChar (Empty,Just b) = toUpper $ boosterChar b
        toChar (Wrapped,Just b) = toLower $ boosterChar b
        boosterChar Extension = 'B'
        boosterChar FastWheel = 'F'
        boosterChar Drill = 'L'
        boosterChar Mysterious = 'X'

countEmpty :: World -> Int
countEmpty World {..} =
  V.sum $ V.map (V.sum . V.map (\(c,_) -> if c==Empty then 1 else 0)) worldMap

countWrapped :: World -> Int
countWrapped World {..} =
  V.sum $ V.map (V.sum . V.map (\(c,_) -> if c==Wrapped then 1 else 0)) worldMap

data Position = Above | Below | RightOf | LeftOf | WTF deriving (Eq, Ord, Read, Show)

botMainHandsPos :: BotState -> Position
botMainHandsPos bot = let
  h = take 3 $ drop 1 $ reverse $ hands bot -- abusing the fact that extra hands are attached at the head
  (xs, ys) = unzip h
  in if | all (1==)  xs -> RightOf
        | all (-1==) xs -> LeftOf
        | all (1==)  ys -> Above
        | all (-1==) ys -> Below
        | otherwise     -> WTF
