{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Heur1 where

import Types
import Rules
import Utils

import Data.Graph.AStar
import Data.Hashable
import qualified Data.HashMap.Strict as M
import Data.HashSet (HashSet, fromList)
import Data.Maybe
import Data.List
import qualified Data.Sequence as Seq
import Data.Sequence (Seq,(><),(|>),empty)
import qualified Data.Vector as V
import GHC.Generics (Generic)

data WS = WS
  { world :: World
  , bot :: BotState
  } deriving (Eq,Show,Read)

data Search = Search
  { sPos :: Pt
  , sAct :: Action
  } deriving (Eq,Ord,Show,Read,Generic)
instance Hashable Search

solve :: Problem -> Maybe Solution
solve = go . start
  where go :: WS -> Maybe Solution
        go ws
          | countEmpty (world ws) == 0 = Just empty
          | otherwise = do
              p <- pathToEmpty (world ws) $ Search { sPos = pos $ bot ws, sAct = Nop }
              let path= Seq.fromList $ fmap sAct p
                  (w',b') = foldl' actionOutcome (world ws, bot ws) path
              (path><) <$> go (WS w' b')

-- | Initial state
start :: Problem -> WS
start p = WS { world = initialWorld, bot = initialBot }
  where
    initialWorld = problemToWorld p
    initialBot   = problemToBotState p

-- | Whether cell is empty (search goal)
goal :: World -> Search -> Bool
goal w s = fst (worldMap w !@ sPos s) == Empty

-- | Distance between *neighbour* states
distance :: Search -> Search -> Int
distance s1 s2 = abs $ dist $ sPos s1 `sub` sPos s2

-- | Distance to the goal (approximate, never overshoot)
heuristic :: World -> Search -> Int
heuristic w s = minimum $
                map (dist . (`sub` sPos s)) $
                concatMap V.toList $
                V.toList $
                V.imap (\y row -> V.map (y,) $ V.findIndices ((/=Stone).fst) row) $
                worldMap w

nextState :: World -> Search -> HashSet Search
nextState world Search{..} = fromList $ fmap nextState' allowed
  where
    nextState' m = let
      d = moveDir m
      next = add sPos d
      in Search{ sPos = next, sAct = m }
    allowed = filter (allowedMove world) actions
    allowedMove w m = let
      d = moveDir m
      next = add sPos d
      in inField world next && not (obstacleOn world next)
    actions =
      [ MoveUp
      , MoveDown
      , MoveLeft
      , MoveRight
      ]

pathToEmpty :: World -> Search -> Maybe [Search]
pathToEmpty w = aStar (nextState w) distance (heuristic w) (goal w)

