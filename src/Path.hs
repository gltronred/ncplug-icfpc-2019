{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Path where

import Types
import Utils
import Rules

import Data.Function
import Data.List
import qualified Data.Map.Lazy as M
import Data.Map.Lazy (Map)
import Data.Maybe
import qualified Data.Sequence as Seq
import Data.Sequence (Seq,empty,(|>))

pathBetween :: World -> Pt -> Pt -> Maybe Solution
pathBetween w@World{..} src dest = case mfinal of
                                     Nothing -> Nothing
                                     Just final -> Just $ extractPath final dest
  where
    initial :: Map Pt Int
    initial = M.singleton src 0
    propagate :: Int -> Map Pt Int -> Maybe (Map Pt Int)
    propagate i acc
      | dest `M.member` acc = Just acc
      | makeStep w i acc == acc = Nothing
      | otherwise = propagate (i+1) $ makeStep w i acc
    mfinal :: Maybe (Map Pt Int)
    mfinal = propagate 0 initial

extractPath :: Map Pt Int -> Pt -> Seq Action
extractPath m cur = case m M.! cur of
  0 -> empty
  _ -> let
    ns = mapMaybe (\p -> (p,) <$> m M.!? p) $ neighbours cur
    prev = fst $ minimumBy (compare`on`snd) ns
    in extractPath m prev |> direction cur prev

neighbours :: Pt -> [Pt]
neighbours (x,y) = [(x-1,y), (x+1,y), (x,y-1), (x,y+1)]

makeStep :: World -> Int -> Map Pt Int -> Map Pt Int
makeStep w k m = M.unionsWith min $ m : map f (filter ((==k).snd) $ M.assocs m)
  where
    f :: (Pt, Int) -> Map Pt Int
    f (p,i) = M.fromList $ map (,i+1) $ filter (not . obstacleOn w) $ neighbours p

direction :: Pt -> Pt -> Action
direction (x,y) (xn, yn)
  | yn > y = MoveDown
  | yn < y = MoveUp
  | xn > x = MoveLeft
  | xn < x = MoveRight

